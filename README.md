# Commerce Adyen Drop-In

The Drupal Commerce Adyen Drop-In module integrates Adyen's payment solution into Drupal's e-commerce platform. The implementation follows the guide published here <https://docs.adyen.com/online-payments/build-your-integration/?platform=Web&integration=Drop-in&version=5.55.1>


## Installation

To install the drupal/commerce_adyen_drop_in module in your Drupal project using Composer, you can follow these quick steps:

### Ensure Composer is Installed
Before beginning, make sure Composer is installed on your system. Composer is a dependency manager for PHP, which Drupal uses. You can check if Composer is installed by running composer --version in your command line. If it's not installed, download and install it from getcomposer.org.

### Navigate to Your Drupal Project Root
Open your command line tool and navigate to the root directory of your Drupal project. This is the directory where your main composer.json file is located.

### Require the Module
Run the following command to require the commerce_adyen_drop_in module in your project:

```bash
composer require drupal/commerce_adyen_drop_in
```


This command tells Composer to find the module, download it, and also manage any dependencies the module might have.

### Enable the Module
After Composer successfully installs the module, you need to enable it in your Drupal site. You can do this through the Drupal admin interface or by using Drush. To enable the module using Drush, run:

```bash
drush en commerce_adyen_drop_in -y
```

## Configuration

### Initial Setup

To configure the Adyen payment gateway in Drupal Commerce, you'll need to gather several key pieces of information from your Adyen account and set up the gateway mode.

#### Adyen Account Details

1. **API Key**: Obtain your unique API Key from your Adyen account. This key is used for server-side communications.
2. **Client Key**: Get the Client Key which is used for client-side interactions.
3. **Merchant Account**: Identify your specific Merchant Account in Adyen.
4. **HMAC Key for Webhook**: Get HMAC key from you webhook configuration (refer to Configuring Adyen Webhook for Authorizations section )
5. **Payment Gateway Mode**: Choose between 'Test' and 'Live' modes in the Drupal Commerce Adyen settings. For testing purposes, use 'Test' mode.
6. **Live Endpoint URL Prefix**: When operating in 'Live' mode, obtain the correct Live Endpoint URL Prefix from your Adyen account: <https://docs.adyen.com/development-resources/live-endpoints/>

#### Drupal Commerce Configuration

- Navigate to the payment gateway configuration section in your Drupal Commerce admin panel.
- Enter the acquired Adyen account details (API Key, Client Key, Merchant Account, etc.).
- Select the appropriate mode (Test or Live) and enter the Live Endpoint URL Prefix if in Live mode.

### Configuring Adyen Webhook for Authorizations

To handle authorization notifications from Adyen, configure the webhook in your Adyen account.

1. **Set Up Notification Webhook**:
   - In your Adyen account, go to the webhook section.
   - Create a new webhook endpoint pointing to your Drupal Commerce endpoint: /payment/notify/{$your_payment_gateway_id}
   - Generate HMAC Key and copy it to your payment gateway configuration.

2. **Configure Event Types**:
   - Choose event types that you want to receive notifications for, such as authorization, payment capture, etc.

3. **Verify & Test**:
   - Ensure the webhook is correctly configured by sending a test notification from Adyen.
   - Verify that your Drupal Commerce site correctly receives and processes the notification.

Remember, accurate configuration of both the payment gateway and the webhook is crucial for smooth payment processing and handling of transactions.
