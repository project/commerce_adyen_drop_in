<?php

namespace Drupal\commerce_adyen_drop_in\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Adyen\Environment;
use Drupal\Core\Url;

/**
 * Class PaymentOffsiteForm.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_adyen_drop_in\Plugin\Commerce\PaymentGateway\DropIn $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Pass Adyen Drop-in configuration data.
    $form['#attached']['drupalSettings']['adyenDropIn']['mode'] = $payment_gateway_plugin->getMode() == 'live' ? Environment::LIVE : Environment::TEST;
    $form['#attached']['drupalSettings']['adyenDropIn']['clientKey'] = $configuration['client_key'];
    $form['#attached']['drupalSettings']['adyenDropIn']['returnUrl'] = $form['#return_url'];

    // Get sessionId and redirectResult if it exists in the request.
    // If the do exist, then we have a redirect from Adyen.
    // We need to handle it accordingly.
    $session_id = \Drupal::request()->get('sessionId');
    $redirect_result = \Drupal::request()->get('redirectResult');
    if (!empty($session_id) && !empty($redirect_result)) {
      $form['#attached']['drupalSettings']['adyenDropIn']['sessionType'] = 'redirect';
      $form['#attached']['drupalSettings']['adyenDropIn']['sessionId'] = $session_id;
      $form['#attached']['drupalSettings']['adyenDropIn']['redirectResult'] = $redirect_result;
    }
    else {
      $session = $payment_gateway_plugin->setUpAdyenSession(
        $payment->getOrder(),
        Url::fromRoute('<current>', [], ['absolute' => TRUE])->toString());
      $form['#attached']['drupalSettings']['adyenDropIn']['sessionType'] = 'new';
      $form['#attached']['drupalSettings']['adyenDropIn']['sessionId'] = $session['id'];
      $form['#attached']['drupalSettings']['adyenDropIn']['sessionData'] = $session['sessionData'];
    }

    $form['#process'][] = [get_class($this), 'processRedirectForm'];

    // Pass session_id to the redirect.
    $form['session_id'] = [
      '#type' => 'hidden',
      '#value' => $session['id'],
      '#parents' => ['session_id'],
    ];

    // Add the drop-in container.
    $form['drop_in_container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'dropin-container',
      ],
    ];

    // Add in Adyen js library.
    $form['#attached']['library'] = ['commerce_adyen_drop_in/drop_in'];

    return $form;
  }

  /**
   * Prepares the complete form for a redirect.
   *
   * Sets the form #action, adds a class for the JS to target.
   * Workaround for buildConfigurationForm() not receiving $complete_form.
   *
   * @param array $form
   *   The plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed form element.
   */
  public static function processRedirectForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    // We make sure that actual Commerce actions are hidden here.
    // The actual redirects are handled via JS Drop-In component.
    $complete_form['actions']['#access'] = TRUE;
    foreach (Element::children($complete_form['actions']) as $element_name) {
      $complete_form['actions'][$element_name]['#access'] = FALSE;
    }

    // Add cancel link to actions.
    $complete_form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => t('Go Back'),
      '#url' => Url::fromUri($form['#cancel_url']),
    ];

    return $form;
  }

}
