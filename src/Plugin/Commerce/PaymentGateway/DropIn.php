<?php

namespace Drupal\commerce_adyen_drop_in\Plugin\Commerce\PaymentGateway;

use Adyen\AdyenException;
use Adyen\Client;
use Adyen\Environment;
use Adyen\Util\HmacSignature;
use Adyen\Service\Checkout;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_payment\Exception\DeclineException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the adyen_drop_in payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "adyen_drop_in",
 *   label = "Adyen Drop-In",
 *   display_label = "Adyen",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_adyen_drop_in\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   js_library = "commerce_adyen_drop_in/drop_in",
 *   requires_billing_information = TRUE,
 * )
 */
class DropIn extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Get the amount number in minor units.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return int
   *   The amount number in minor units.
   */
  private static function getMinorUnits(Price $amount) {
    $currency_code = $amount->getCurrencyCode();
    $number = $amount->getNumber();
    $minor_unit = self::getMinorUnitsMap()[$currency_code] ?? 2;
    return $number * (10 ** $minor_unit);
  }

  /**
   * Get the Price from minor units.
   *
   * @param int $minor_units_amount
   *   The amount in minor units.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public static function getPrice($minor_units_amount, $currency_code) {
    $minor_unit = self::getMinorUnitsMap()[$currency_code] ?? 2;
    return new Price($minor_units_amount / (10 ** $minor_unit), $currency_code);
  }

  /**
   * The minor unit map.
   *
   * @return int[]
   *   The minor unit map.
   */
  private static function getMinorUnitsMap(): array {
    return [
      'AED' => 2,
      'ALL' => 2,
      'AMD' => 2,
      'ANG' => 2,
      'AOA' => 2,
      'ARS' => 2,
      'AUD' => 2,
      'AWG' => 2,
      'AZN' => 2,
      'BAM' => 2,
      'BBD' => 2,
      'BDT' => 2,
      'BGN' => 2,
      'BHD' => 3,
      'BMD' => 2,
      'BND' => 2,
      'BOB' => 2,
      'BRL' => 2,
      'BSD' => 2,
      'BWP' => 2,
      'BYN' => 2,
      'BZD' => 2,
      'CAD' => 2,
      'CHF' => 2,
      'CLP' => 2,
      'CNY' => 2,
      'COP' => 2,
      'CRC' => 2,
      'CUP' => 2,
      'CVE' => 0,
      'CZK' => 2,
      'DJF' => 0,
      'DKK' => 2,
      'DOP' => 2,
      'DZD' => 2,
      'EGP' => 2,
      'ETB' => 2,
      'EUR' => 2,
      'FJD' => 2,
      'FKP' => 2,
      'GBP' => 2,
      'GEL' => 2,
      'GHS' => 2,
      'GIP' => 2,
      'GMD' => 2,
      'GNF' => 0,
      'GTQ' => 2,
      'GYD' => 2,
      'HKD' => 2,
      'HNL' => 2,
      'HTG' => 2,
      'HUF' => 2,
      'IDR' => 0,
      'ILS' => 2,
      'INR' => 2,
      'IQD' => 3,
      'ISK' => 2,
      'JMD' => 2,
      'JOD' => 3,
      'JPY' => 0,
      'KES' => 2,
      'KGS' => 2,
      'KHR' => 2,
      'KMF' => 0,
      'KRW' => 0,
      'KWD' => 3,
      'KYD' => 2,
      'KZT' => 2,
      'LAK' => 2,
      'LBP' => 2,
      'LKR' => 2,
      'LYD' => 3,
      'MAD' => 2,
      'MDL' => 2,
      'MKD' => 2,
      'MMK' => 2,
      'MNT' => 2,
      'MOP' => 2,
      'MRU' => 2,
      'MUR' => 2,
      'MVR' => 2,
      'MWK' => 2,
      'MXN' => 2,
      'MYR' => 2,
      'MZN' => 2,
      'NAD' => 2,
      'NGN' => 2,
      'NIO' => 2,
      'NOK' => 2,
      'NPR' => 2,
      'NZD' => 2,
      'OMR' => 3,
      'PAB' => 2,
      'PEN' => 2,
      'PGK' => 2,
      'PHP' => 2,
      'PKR' => 2,
      'PLN' => 2,
      'PYG' => 0,
      'QAR' => 2,
      'RON' => 2,
      'RSD' => 2,
      'RUB' => 2,
      'RWF' => 0,
      'SAR' => 2,
      'SBD' => 2,
      'SCR' => 2,
      'SEK' => 2,
      'SGD' => 2,
      'SHP' => 2,
      'SLE' => 2,
      'SOS' => 2,
      'SRD' => 2,
      'STN' => 2,
      'SVC' => 2,
      'SZL' => 2,
      'THB' => 2,
      'TND' => 3,
      'TOP' => 2,
      'TRY' => 2,
      'TTD' => 2,
      'TWD' => 2,
      'TZS' => 2,
      'UAH' => 2,
      'UGX' => 0,
      'USD' => 2,
      'UYU' => 2,
      'UZS' => 2,
      'VEF' => 2,
      'VND' => 0,
      'VUV' => 0,
      'WST' => 2,
      'XAF' => 0,
      'XCD' => 2,
      'XOF' => 0,
      'XPF' => 0,
      'YER' => 2,
      'ZAR' => 2,
      'ZMW' => 2,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory */
    $logger_factory = $container->get('logger.factory');
    $instance->logger = $logger_factory->get('commerce_adyen_drop_in');
    $instance->languageManager = $container->get('language_manager');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->moduleHandler = $container->get('module_handler');

    return $instance;
  }

  /**
   * Gets client.
   *
   * @return \Adyen\Client
   *   The client object.
   */
  private function getClient() {
    $client = new Client();
    $client->setApplicationName($this->configuration['application_name']);
    $client->setEnvironment($this->getMode() == 'live' ? Environment::LIVE : Environment::TEST, $this->configuration['live_endpoint_url_prefix']);
    $client->setXApiKey($this->configuration['api_key']);
    $client->setTimeout($this->configuration['timeout']);
    return $client;
  }

  /**
   * Sets up Adyen Session.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $redirect_url
   *   The redirect URL.
   *
   * @return mixed
   *   The session.
   */
  public function setUpAdyenSession(OrderInterface $order, $redirect_url) {
    // Get billing profile and address.
    $billing_profile = $order->getBillingProfile();
    if (!$billing_profile) {
      throw new PaymentGatewayException('Billing profile not found.');
    }
    $address = $billing_profile->get('address')->first();

    // Get current language.
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    // If user is authenticated, use user ID as shopperReference.
    // If user is anonymous, use order ID as shopperReference.
    // This is to prevent duplicate shopperReference.
    $user = $order->getCustomer();

    // Check if user is authenticated.
    if ($user->isAuthenticated()) {
      $shopper_reference = 'u-' . $user->uuid();
    }
    else {
      $shopper_reference = 'o-' . $order->uuid();
    }

    $client = $this->getClient();
    $service = new Checkout($client);
    $params = [
      'amount' => [
        'currency' => $order->getTotalPrice()->getCurrencyCode(),
        'value' => self::getMinorUnits($order->getTotalPrice()),
      ],
      'channel' => 'Web',
      'mode' => 'embedded',
      'merchantAccount' => $this->configuration['merchant_account'],
      'countryCode' => $address->getCountryCode(),
      'shopperName' => [
        'firstName' => $address->getGivenName(),
        'lastName' => $address->getFamilyName(),
      ],
      'billingAddress' => [
        'street' => $address->getAddressLine1(),
        'houseNumberOrName' => $address->getAddressLine2(),
        'city' => $address->getLocality(),
        'stateOrProvince' => $address->getAdministrativeArea(),
        'postalCode' => $address->getPostalCode(),
        'country' => $address->getCountryCode(),
      ],
      'shopperEmail' => $order->getEmail(),
      'shopperReference' => $shopper_reference,
      'shopperLocale' => $langcode,
      'shopperIP' => $this->request->getClientIp(),
      'shopperInteraction' => 'Ecommerce',
      'reference' => $order->id(),
      'returnUrl' => $redirect_url,
      // This is different from Commerce's #return_url.
    ];

    // Enable modules to alter the session data.
    $payment_gateway_id = $this->parentEntity->id();
    $this->moduleHandler->alter('commerce_adyen_drop_in_session_params', $params, $order, $payment_gateway_id);

    // @todo Add Line Items - lineItems.
    try {
      $session = $service->sessions($params);
    }
    catch (AdyenException $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException($e->getMessage());
    }
    return $session;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'application_name' => 'Drupal Commerce',
      'api_key' => '',
      'client_key' => '',
      'hmac_key' => '',
      'merchant_account' => '',
      'timeout' => 30,
      'live_endpoint_url_prefix' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['live_endpoint_url_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Live endpoint url prefix'),
      '#default_value' => $this->configuration['live_endpoint_url_prefix'],
      '#description' => $this->t('The prefix only. Do not provide a full or partial url. Obtain this from your Adyen account. Follow these <a href="https://docs.adyen.com/development-resources/live-endpoints/" target="_blank">directions</a> to locate your prefix.'),
      '#states' => [
        'enabled' => [
          ':input[name="configuration[adyen_drop_in][mode]"]' => ['value' => 'live'],
        ],
        'required' => [
          ':input[name="configuration[adyen_drop_in][mode]"]' => ['value' => 'live'],
        ],
      ],
      '#attributes' => [
        'pattern' => '^[^./ ]+$',
        'title' => $this->t('The live prefix only. Do not include a partial or full url.'),
      ],
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $this->configuration['api_key'],
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    $form['client_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client key'),
      '#default_value' => $this->configuration['client_key'],
      '#required' => TRUE,
    ];

    $form['hmac_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HMAC key'),
      '#default_value' => $this->configuration['hmac_key'],
      '#required' => TRUE,
    ];

    $form['merchant_account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant account'),
      '#default_value' => $this->configuration['merchant_account'],
      '#required' => TRUE,
    ];

    $form['timeout'] = [
      '#type' => 'number',
      '#min' => -1,
      '#max' => 60,
      '#step' => 1,
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('The timeout in seconds.'),
      '#default_value' => $this->configuration['timeout'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['client_key'] = $values['client_key'];
      $this->configuration['hmac_key'] = $values['hmac_key'];
      $this->configuration['merchant_account'] = $values['merchant_account'];
      $this->configuration['timeout'] = $values['timeout'];
      $this->configuration['live_endpoint_url_prefix'] = $values['live_endpoint_url_prefix'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // Handle negative result codes first.
    $result_code = $request->query->get('resultCode');
    if (!$result_code) {
      throw new PaymentGatewayException('Result code not provided.');
    }

    if (!in_array($result_code, ['Authorised', 'Pending', 'Received'])) {
      switch ($result_code) {
        case 'Refused':
          throw new DeclineException('Payment declined.');

        default:
          // Unknown result code.
          throw new PaymentGatewayException('Unknown error has occured.');
      }
    }

    // Get session ID from request.
    $session_id = $request->query->get('sessionId');
    if (!$session_id) {
      throw new PaymentGatewayException('Session ID not provided.');
    }

    // Get sessionResult from request.
    $session_result = $request->query->get('sessionResult');
    if (!$session_result) {
      throw new PaymentGatewayException('Session result not provided.');
    }

    // Check if payment session is completed in Adyen.
    $client = $this->getClient();
    $service = new Checkout($client);

    // Get url prefix from service.
    $endpoint = $client->getConfig()->get('endpointCheckout');
    $version = $client->getApiCheckoutVersion();
    $url = $endpoint . '/' . $version . '/sessions/' . $session_id;
    $curlClient = $client->getHttpClient();
    try {
      $response = $curlClient->requestHttp($service, $url, ['sessionResult' => $session_result], 'get');

      // Call hook that informs other modules that payment is completed.
      $payment_gateway_id = $this->parentEntity->id();
      $this->moduleHandler->invokeAll('commerce_adyen_drop_in_return', [
        $payment_gateway_id,
        $order,
        $response,
      ]);

      switch ($response['status']) {
        case 'completed':
        case 'paymentPending':
          // We don't update or create payment object here yet.
          // In both cases, we wait for onNotify to update the payment object.
          // This is according to how Adyen recommends to handle payments.
          // Please note that for this to work,
          // AUTHORIZATION webhook must be correctly set up.
          break;

        case 'canceled':
          throw new PaymentGatewayException('Session canceled.');

        case 'expired':
          throw new PaymentGatewayException('Session expired.');

        default:
          throw new PaymentGatewayException('Unknown session status: ' . $response['status']);
      }
    }
    catch (AdyenException $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    // Get payload posted to the endpoint.
    $payload = $request->getContent();
    if (empty($payload)) {
      return new Response('Adyen notification payload not provided.', 400);
    }

    // Extract JSON from payload.
    $json = json_decode($payload, TRUE);
    if (!$json) {
      return new Response('Notification JSON provided not valid.', 400);
    }

    // Initialise HmacSignature library from Adyen.
    $hmac = new HmacSignature();

    // Fetch HMAC key.
    $hmac_key = $this->configuration['hmac_key'];

    // Cycle through the notification items received.
    foreach ($json['notificationItems'] as $notification_item) {
      $notification_item = $notification_item['NotificationRequestItem'];

      // Check the signature for this item.
      if (!$hmac->isValidNotificationHMAC($hmac_key, $notification_item)) {
        $this->logger->error('Unable to validate the HMAC signature.');
        continue;
      }

      // Check the event code.
      $event_code = $notification_item['eventCode'];
      if ($event_code != 'AUTHORISATION') {
        $this->logger->error('Notification event code not AUTHORISATION: ' . $event_code);
        continue;
      }

      // Check the success state.
      $success = filter_var($notification_item['success'], FILTER_VALIDATE_BOOLEAN);
      if (!$success) {
        $this->logger->error('Notification not successful: ' . $success);
        continue;
      }

      // Get order ID from JSON.
      $order_id = $notification_item['merchantReference'];
      $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
      if (!$order) {
        $this->logger->error('Notification order not found: ' . $order_id);
        continue;
      }

      // Get pspReference from request.
      $psp_reference = $notification_item['pspReference'];

      // Check if the completed payment with this ID already exists
      // and if it is in a completed state.
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->loadByRemoteId($psp_reference);
      if ($payment && $payment->getState() == 'completed') {
        $this->logger->error('Notification payment already exists: ' . $psp_reference);
        continue;
      }

      // Get the currency and amount.
      $amount = $notification_item['amount']['value'];
      $currency = $notification_item['amount']['currency'];

      // Get the reason.
      $reason = $notification_item['reason'];

      // Calculate event data timestamp.
      $event_date = $notification_item['eventDate'];

      // Change ISO event date to timestamp.
      $event_time = strtotime($event_date);

      // Create payment.
      $payment = $payment_storage->create([
        'state' => 'completed',
        'amount' => $this->getPrice($amount, $currency),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order_id,
        'remote_id' => $psp_reference,
        'remote_state' => $reason,
        'authorized' => $event_time,
      ]);
      $payment->save();
    }

    // Respond with HTTP 200 OK and accept in body.
    return new Response('[accepted]', 200);
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, ?Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    try {
      $remote_id = $payment->getRemoteId();
      $currency_code = $amount->getCurrencyCode();
      $params = [
        'paymentPspReference' => $remote_id,
        'merchantAccount' => $this->configuration['merchant_account'],
        'amount' => [
          'currency' => $currency_code,
          'value' => self::getMinorUnits($amount),
        ],
      ];

      $client = $this->getClient();
      $service = new Checkout($client);
      $service->refunds($params);
    }
    catch (AdyenException $e) {
      throw new PaymentGatewayException($e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

}
