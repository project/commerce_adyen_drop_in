/**
 * @file
 * Defines behaviors for the Adyen Drop-In form.
 */

(($, Drupal, drupalSettings, AdyenCheckout) => {
  Drupal.adyenDropIn = Drupal.adyenDropIn || {};

  Drupal.adyenDropIn.getAdyenCheckout = async function (session) {
    const configuration = {
      environment: drupalSettings.adyenDropIn.mode,
      clientKey: drupalSettings.adyenDropIn.clientKey,
      analytics: {
        enabled: true,
      },
      session: session,
      showStoredPaymentMethods: true,
      showRemovePaymentMethodButton: true,
      onPaymentCompleted: (result, component) => {
        var sessionId = null;
        var sessionResult = null;
        var resultCode = result.resultCode;

        // Check the status
        switch (result.resultCode) {
          case 'Authorised':
          case 'Pending':
          case 'Received':
            sessionId = session.id;
            sessionResult = result.sessionResult;
            break;
        }

        window.location.href = drupalSettings.adyenDropIn.returnUrl
          + "?resultCode=" + resultCode
          + "&sessionId=" + sessionId
          + "&sessionResult=" + sessionResult;
      },
      onError: (error, component) => {
        console.error(error.name, error.message, error.stack, component);

        const messages = new Drupal.Message();
        messages.clear();
        messages.add(error.message, { type: 'error' });

        $('html, body').animate(
          {
            scrollTop: $('[role="alert"]').offset().top - 200,
          },
          1000,
        );
      }
    };

    // Create an instance of AdyenCheckout using the configuration object.
    return await AdyenCheckout(configuration);
  };

  Drupal.behaviors.adyenDropIn = {
    async attach(context) {
      once('adyen-drop-in', '#dropin-container').forEach(async () => {
        const sessionType = drupalSettings.adyenDropIn.sessionType;
        if (sessionType == 'new') {
          const checkout = await Drupal.adyenDropIn.getAdyenCheckout({
            id: drupalSettings.adyenDropIn.sessionId,
            sessionData: drupalSettings.adyenDropIn.sessionData
          });

          // Create an instance of Drop-in and mount it to the container you created.
          checkout.create('dropin').mount('#dropin-container');
        } else {
          const checkout = await Drupal.adyenDropIn.getAdyenCheckout({
            id: drupalSettings.adyenDropIn.sessionId
          });

          // Submit the redirectResult value you extracted from the returnUrl.
          checkout.submitDetails({ details: {
            redirectResult: drupalSettings.adyenDropIn.redirectResult } });
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings, window.AdyenCheckout);
